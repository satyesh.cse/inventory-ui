import { Component, OnInit } from '@angular/core';
import { InventoryService } from '../inventory.service';
import { InventoryInfo } from './InventoryInfo';

@Component({
  selector: 'app-inventorybody',
  templateUrl: './inventorybody.component.html',
  styleUrls: ['./inventorybody.component.css']
})
export class InventorybodyComponent implements OnInit {

  selection: string;
  searchtext: string = '';
  searchresponse: Array<InventoryInfo>;
  editresponse: string;
  addresponse: string;
  togglecreate: boolean = false;
  toggleEdit: boolean = false;
  createButtonText: string = 'Expand Creation Form';
  changeInventory: InventoryInfo;
  addInventory: InventoryInfo;
  searchText: string = 'Search';
  searchErrorText: String;
  constructor(private invservice: InventoryService) {
    this.changeInventory = new InventoryInfo();
    this.addInventory = new InventoryInfo();
  }

  ngOnInit() {
  }

  search() {
    this.searchErrorText = null;
    this.searchText = 'Searching';
    this.toggleEdit = false;
    console.log("search called");
    this.searchresponse = [];
    this.invservice.search(this.selection, this.searchtext).subscribe((response: InventoryInfo[]) => {
      if(response instanceof Array){
        this.searchresponse = response;
        console.log('Array of response'+this.searchresponse.toString());
      }
      else{
        this.searchresponse.push(response);
        console.log('single response'+this.searchresponse.toString);
      }

    },
      (err: any) => {
        console.log("err" + err);
        if (err.status == 404) {
          this.searchErrorText = ' ¯\_(ツ)_/¯ No Such Item Exists';
        }
        else if (err.status == 500) {
          this.searchErrorText = '(`_`) Server is crazy';
        }
        else {
          this.searchErrorText = '(*_*) Server is Offline';
        }
      }
    );
    this.searchText = 'Search';
  }


   edititem(){
    this.editresponse='ss';
     this.invservice.edit(this.changeInventory).subscribe((response : any) =>{
           this.editresponse='applied';
           console.log('reponse='+response);

     },
     (err: any) =>{
      this.editresponse='failed';
     });
   }

   additem()
   {
    this.addresponse='';
     this.invservice.add(this.addInventory).subscribe((response :any)=>{
      this.addresponse='Created :)';
      console.log('created');
     },
     (err: any) =>{
      this.addresponse='failed';
     });
   }




  toggleAdditem() {
    if (this.togglecreate == false) {
      this.createButtonText = 'Collapse Creation Form';
      this.togglecreate = true;
    }
    else {
      this.createButtonText = 'Expand Creation Form';
      this.togglecreate = false;
    }
  }
  fillEditItem(items: any) {
    console.log("filling items");
    console.log(items);
    this.toggleEdit = true;
    this.editresponse='';

    this.changeInventory.itemcode = items.itemcode;
    this.changeInventory.itemName = items.itemName;
    this.changeInventory.itemType = items.itemType;
    this.changeInventory.stockLevel = items.stockLevel;
    this.changeInventory.itemPrice = items.itemPrice;
    this.changeInventory.itemSellPrice = items.itemSellPrice;
    this.changeInventory.useByDate = items.useByDate;

  }

  
  handleError(err: any) {
    console.log("err" + err);
    if (err.status == 404) {
      this.searchErrorText = ' ¯\_(ツ)_/¯ No Such Item Exists';
    }
    else if (err.status == 500) {
      this.searchErrorText = '(`_`) Server is crazy';
    }
    else {
      this.searchErrorText = '(*_*) Server is Offline';
    }
  }
}
