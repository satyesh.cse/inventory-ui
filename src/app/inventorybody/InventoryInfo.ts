export class InventoryInfo
{
 itemcode : string;
 itemName : string;
 itemType : string;
 stockLevel : string;
 itemPrice : string;
 itemSellPrice : string;
 useByDate : string;
}