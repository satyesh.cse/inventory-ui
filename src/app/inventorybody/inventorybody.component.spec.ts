import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventorybodyComponent } from './inventorybody.component';

describe('InventorybodyComponent', () => {
  let component: InventorybodyComponent;
  let fixture: ComponentFixture<InventorybodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventorybodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventorybodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
