import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Observable } from 'rxjs';
import { InventoryInfo } from './inventorybody/InventoryInfo';

@Injectable({
  providedIn: 'root'
})
export class InventoryService {

  private baseurl : string ='http://localhost:8080/inventory/stock/';
  constructor(private client:HttpClient) { }

  search(criteria :string='code',filter:string)   : Observable<any>{
    console.log("service called");
    let obs=this.client.get(this.baseurl+filter);
   return obs
    }
    edit(changeInventory: InventoryInfo) : Observable<any>
    {
      console.log("edit called");
      let obs=this.client.put(this.baseurl+changeInventory.itemcode,changeInventory);
      return obs;
    }
    add(addInventory : InventoryInfo) : Observable<any> {
       console.log("Add Item called");
       let obs=this.client.post(this.baseurl,addInventory);
       return obs;

    }

  }
  
